const burgerMenu = document.getElementById("burger");
const navbarMenu = document.getElementById("header-nav-list");

// Show and Hide Navbar Menu
burgerMenu.addEventListener("click", () => {
	burgerMenu.classList.toggle("is-active");
	navbarMenu.classList.toggle("is-active");
});