/*
    1. Lazy load scripts when needed
    Browsers run JavaScript before creating the DOM tree and 
    painting the pixels on the screen, because scripts can add 
    new elements to the page or change the layout or style of some DOM nodes. 
    So, by giving the browser less scripts to execute during page load, we can reduce 
    the time it takes for the final DOM tree creation and painting, after which the user will be able to see the page.

    One way to do this in jQuery is by using $.getScript to load any script file at the time of its need rather than during page load.
*/


// main js
$.getScript( "assets/js/main.js");

/*
    It’s an ajax function that will get a single script file when you want it, 
    but note that the file fetched isn’t cached. To enable caching for getScript you’ll 
    have to enable the same for all of the ajax requests. You can do so by using the code below:
*/

$.ajaxSetup({
    cache: true
});